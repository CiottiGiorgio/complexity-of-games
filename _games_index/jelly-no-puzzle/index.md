---
title: Jelly-No Puzzle 1-color
short_description: Make sure to merge all the jellies of the same color. Use gravity at your advantage.
---

## Description

[Jelly-No Puzzle][1] is a game where the player is presented with a rectangular level consisting of unmovable walls and jellies of various colors.

The goal of the game is to make sure that all the jellies of the same color are merged. In this game jellies fall due to gravity and push each other.

Gravity will make a jelly fall is every unit square is able to move below and jellies can push each other if every jelly is able to move in the pushing direction. No Jelly can ever move upwards.

![](example_level.jpeg){:width="700"}

## Known complexity results

This game is NP-complete as illustrated in this paper.

The reduction starts from 3PARTITION and constructs a level that is solvable if and only if an exact 3-partition of the input multiset exists.

It is conjectured by the author that multi-color version of this game does not belong in NP.

A playable version of this reduction is available [here]({{site.baseurl}}/g/jelly-no-puzzle/).

[1]: https://avorobey.github.io/jelly/
